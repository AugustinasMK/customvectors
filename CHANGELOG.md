# Changerlog v1.0 (2018-05-27)
## Pridėta
- Implementuotas FastVector, gebantis nugalėti std::vector visais atvejais, jei dirbama su baziniais duomenų tipais.
- Implementuotas StrongVector, gebantis dirbti su visais duomenų tipais ir dirbantis panašia sparta kaip ir std::vector.
- Parašyta spartos analizė