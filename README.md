# Augustino Makevičiaus objektinio programavimo 4 užduotis. (VU ISI 1 kursas)
## Programos tikslas
Reikalauta sukurti kuo tikslesnę ```std::vector``` implementaciją ir palyginti sukurto vektoriaus spartą su ```std::vector```.
## Programos kodas
Siekiant darbo apsaugojimo nuo plagijavimo, kodas perkėltas į privačią repozitoriją. Norėdami pamatyti kodą, susisiekite adresu **augustinasmkvs.real@gmail.com**
## Programos kūrimas
Sukūrus MyVector susidūriau su problemomis alokuojant string tipo duomenis, tad teko sukurti StrongVector, kuriame pralaimime šiek tiek spartos, bet vektorius veikia su visais duomenų tipais.
## Spartos analizė naudojant int tipo duomenis
1. Debug konfiguracija

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 5.009e-005 s. | 0.00015036 s. |
| n = 100000 | 0.0009015 s. | 0.00195524 s. |
| n = 1000000 | 0.00727911 s. | 0.0183673 s. |
| n = 10000000 | 0.0869584 s. | 0.195733 s. |
| n = 100000000 | 0.816627 s. | 1.97538 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00386055 s. | 0.0002509 s. |
| n = 100000 | 0.00190297 s. | 0.00185671 s. |
| n = 1000000 | 0.0185589 s. | 0.0184666 s. |
| n = 10000000 | 0.202555 s. | 0.200858 s. |
| n = 100000000 | 2.00399 s. | 1.99007 s. |


2. Release konfiguracija nenurodant optimatizavimo flag'o (-O3 -DNDEBUG)

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00010101 s. | 9.944e-005 s. |
| n = 100000 | 0.00045159 s. | 0.00055147 s. |
| n = 1000000 | 0.00350957 s. | 0.00471026 s. |
| n = 10000000 | 0.0467975 s. | 0.0584776 s. |
| n = 100000000 | 0.429985 s. | 0.546494 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00130823 s. | 0.00010027 s. |
| n = 100000 | 0.00044942 s. | 0.00060286 s. |
| n = 1000000 | 0.00356599 s. | 0.00452925 s. |
| n = 10000000 | 0.0497329 s. | 0.0589802 s. |
| n = 100000000 | 0.437506 s. | 0.554439 s. |


3. Release su O0 optimatizavimu

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00010027 s. | 0.0003008 s. |
| n = 100000 | 0.00085233 s. | 0.00200533 s. |
| n = 1000000 | 0.00721792 s. | 0.0182986 s. |
| n = 10000000 | 0.0867059 s. | 0.197378 s. |
| n = 100000000 | 0.830045 s. | 1.93923 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00386517 s. | 0.00020066 s. |
| n = 100000 | 0.001907 s. | 0.00195756 s. |
| n = 1000000 | 0.0185632 s. | 0.01797 s. |
| n = 10000000 | 0.198327 s. | 0.199116 s. |
| n = 100000000 | 1.97819 s. | 1.94485 s. |

4. Release su O1 optimatizavimu

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 9.79e-005 s. | 0.00010027 s. |
| n = 100000 | 0.00050129 s. | 0.00060168 s. |
| n = 1000000 | 0.00392071 s. | 0.00511889 s. |
| n = 10000000 | 0.0475317 s. | 0.0659436 s. |
| n = 100000000 | 0.438834 s. | 0.630245 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00135487 s. | 0.00010015 s. |
| n = 100000 | 0.00055134 s. | 0.00080218 s. |
| n = 1000000 | 0.00422628 s. | 0.00533302 s. |
| n = 10000000 | 0.0517094 s. | 0.0696669 s. |
| n = 100000000 | 0.475739 s. | 0.626991 s. |
 

5. Release su O2 optimatizavimu

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 4.784e-005 s. | 5.325e-005 s. |
| n = 100000 | 0.00044804 s. | 0.00050138 s. |
| n = 1000000 | 0.00356387 s. | 0.00452574 s. |
| n = 10000000 | 0.0510928 s. | 0.0588259 s. |
| n = 100000000 | 0.434793 s. | 0.545502 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00120189 s. | 9.848e-005 s. |
| n = 100000 | 0.00045124 s. | 0.00055135 s. |
| n = 1000000 | 0.00347638 s. | 0.0044233 s. |
| n = 10000000 | 0.05155 s. | 0.0612398 s. |
| n = 100000000 | 0.444781 s. | 0.563254 s. |


6. Release su O3 optimatizavimu

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 9.884e-005 s. | 0.00010311 s. |
| n = 100000 | 0.00050047 s. | 0.00054854 s. |
| n = 1000000 | 0.00391174 s. | 0.00443114 s. |
| n = 10000000 | 0.0484398 s. | 0.0595871 s. |
| n = 100000000 | 0.443847 s. | 0.548075 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00120075 s. | 9.782e-005 s. |
| n = 100000 | 0.00050149 s. | 0.00060345 s. |
| n = 1000000 | 0.00320832 s. | 0.00441934 s. |
| n = 10000000 | 0.0465356 s. | 0.0580648 s. |
| n = 100000000 | 0.42465 s. | 0.537698 s. |


7. Release su Ofast optimatizavimu

|Duomenu kiekis                    |Mano Vektorius   |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 4.895e-005 s. | 0.0001003 s. |
| n = 100000 | 0.00050129 s. | 0.00060334 s. |
| n = 1000000 | 0.00356269 s. | 0.00451019 s. |
| n = 10000000 | 0.0474262 s. | 0.0584089 s. |
| n = 100000000 | 0.427335 s. | 0.542764 s. |

|Duomenu kiekis                    |StrongVector     |std::vector     |
|----------------------------------|-----------------|----------------|
| n = 10000 | 0.00110024 s. | 0.00010098 s. |
| n = 100000 | 0.00050299 s. | 0.00050078 s. |
| n = 1000000 | 0.00361209 s. | 0.00420885 s. |
| n = 10000000 | 0.0464361 s. | 0.0578758 s. |
| n = 100000000 | 0.433462 s. | 0.536948 s. |


## Spartos analizė naudojant [Studento](https://github.com/AugustinasMKVU/AntraUzduotisOOP/blob/master/AntraUzduotis/headers/Studentas.h) tipo duomenis
### Pirma lentelė - std::vector, antra - Strong Vector.
1. Debug konfiguracija

|Duomenu kiekis                    |Duomenu nuskaitymas|Rusiavimas    |
|----------------------------------|-------------------|--------------|
| n = 10 ^ 1 | 0.0015072 s. | 0 s. |
| n = 10 ^ 2 | 0.0035006 s. | 0 s. |
| n = 10 ^ 3 | 0.0240169 s. | 0.000533 s. |
| n = 10 ^ 4 | 0.213566 s. | 0.0065173 s. |
| n = 10 ^ 5 | 2.12124 s. | 0.0651472 s. |

|Duomenu kiekis                    |Duomenu nuskaitymas|Rusiavimas    |
|----------------------------------|-------------------|--------------|
| n = 10 ^ 1 | 0.0010213 s. | 0 s. |
| n = 10 ^ 2 | 0.0030211 s. | 0 s. |
| n = 10 ^ 3 | 0.0230593 s. | 0.0010047 s. |
| n = 10 ^ 4 | 0.213566 s. | 0.012032 s. |
| n = 10 ^ 5 | 2.10259 s. | 0.115338 s. |



2. Release konfiguracija nenurodant optimatizavimo flag'o

|Duomenu kiekis                    |Duomenu nuskaitymas|Rusiavimas    |
|----------------------------------|-------------------|--------------|
| n = 10 ^ 1 | 0.0010026 s. | 0 s. |
| n = 10 ^ 2 | 0.0040197 s. | 0 s. |
| n = 10 ^ 3 | 0.021056 s. | 0 s. |
| n = 10 ^ 4 | 0.204623 s. | 0.001504 s. |
| n = 10 ^ 5 | 2.0743 s. | 0.0195484 s. |

|Duomenu kiekis                    |Duomenu nuskaitymas|Rusiavimas    |
|----------------------------------|-------------------|--------------|
| n = 10 ^ 1 | 0.0010022 s. | 0 s. |
| n = 10 ^ 2 | 0.0030096 s. | 0 s. |
| n = 10 ^ 3 | 0.0200537 s. | 0 s. |
| n = 10 ^ 4 | 0.197527 s. | 0.003008 s. |
| n = 10 ^ 5 | 1.99733 s. | 0.0340906 s. |



