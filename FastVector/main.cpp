#include <iostream>
#include <vector>
#include "Vector.h"
#include "Timer.h"
using namespace std;

class TestType{
/// Studento vardas
    string vardas_{};
    /// Studnto pavardė
    string pavard_{};
    /// Studento namų darbų įverčiai
    vector<double> nDarbai_{};
    /// Studento gautas egzamino įvertinimas
    double egzam_;
};

int main() {
    double spent = 0;

    cout << "|Duomenu kiekis                    |Mano Vektorius   |std::vector     |" <<  endl;
    cout << "|----------------------------------|-----------------|----------------|" <<  endl;
    cout << "| n = 10000 |";
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        Vector<TestType> v2;
        for (int i = 1; i <= 10000; ++i)
            v2.push_back(TestType());
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        std::vector<int> v2;
        for (int i = 1; i <= 10000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |" << endl;
    cout << "| n = 100000 |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        Vector<int> v2;
        for (int i = 1; i <= 100000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        std::vector<int> v2;
        for (int i = 1; i <= 100000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |" << endl;
    cout << "| n = 1000000 |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        Vector<int> v2;
        for (int i = 1; i <= 1000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        std::vector<int> v2;
        for (int i = 1; i <= 1000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |" << endl;
    cout << "| n = 10000000 |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        Vector<int> v2;
        for (int i = 1; i <= 10000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        std::vector<int> v2;
        for (int i = 1; i <= 10000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |" << endl;
    cout << "| n = 100000000 |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        Vector<int> v2;
        for (int i = 1; i <= 100000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |";
    spent = 0;
    for (int j = 0; j < 10; ++j) {
        Timer t{};
        std::vector<int> v2;
        for (int i = 1; i <= 100000000; ++i)
            v2.push_back(i);
        spent += t.getTime();
    }
    cout << " " << spent/10 << " s. |" << endl;


    cout << "return'as" << endl;
    return 0;
}