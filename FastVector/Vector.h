//
// Created by augus on 18/04/26.
//

#ifndef MYVECTOR_VECTOR_H
#define MYVECTOR_VECTOR_H

#include <initializer_list>
#include <memory>
#include <algorithm>

template <class T, class Allocator = std::allocator<T>>
class Vector {
public:
    // types:
    typedef T value_type;
    typedef std::size_t size_type;
    typedef Allocator allocator_type;
    typedef std::ptrdiff_t diff_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef typename std::allocator_traits<Allocator>::pointer pointer;
    typedef typename std::allocator_traits<Allocator>::const_pointer const_pointer;
    typedef T* iterator;
    typedef const T* const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    typedef typename std::allocator_traits<Allocator>::template
            __rebind<T>::other _std_alloc_type;

private:
    size_type cap;
    size_type sz;
    T* elem;

    Allocator alokatorius{};

public:
    // Konstruktoriai ir desktruktorius
    Vector() noexcept : sz(0), cap(0) { elem = alokatorius.allocate(0); }
    explicit Vector(const Allocator& alloc) : sz(0), cap(0), alokatorius(alloc){ elem = alokatorius.allocate(0); }
    Vector(size_type size,const T& value, const Allocator& alloc = Allocator()) : sz(size), cap(size), alokatorius(alloc) {
        elem = alokatorius.allocate(size);
        std::fill(elem,elem + size, value);
    }
    explicit Vector(size_type size, const Allocator& alloc = Allocator()) : sz(size), cap(size), alokatorius(alloc){
        elem = alokatorius.allocate(size);
        std::fill(elem,elem + size, T{});
    }
    Vector(iterator first, iterator last, const Allocator& alloc = Allocator()) : sz(std::distance(first, last)), cap(std::distance(first, last)), alokatorius(alloc){
        elem = alokatorius.allocate(sz);
        std::copy(first,last,elem);
    }
    Vector(const Vector& other) : sz(other.sz), cap(other.sz), alokatorius(std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.alokatorius)){
        elem = alokatorius.allocate(sz);
        std::copy(other.elem,other.elem + other.sz,elem);
    }
    Vector(const Vector& other, const Allocator& alloc) : sz(other.sz), cap(other.sz), alokatorius(std::allocator_traits<allocator_type>::select_on_container_copy_construction(alloc)){
        elem = alokatorius.allocate(sz);
        std::copy(other.elem,other.elem + other.sz,elem);
    }
    Vector(Vector&& other) noexcept : sz(std::move(other.sz)), cap(std::move(other.cap)), alokatorius(alokatorius = std::move(other.alokatorius)), elem(other.elem) {
        other.sz = 0;
        other.cap = 0;
        other.elem = nullptr;
    }
    Vector(std::initializer_list<T> init, const Allocator& alloc = Allocator()) : sz(init.size()), cap(init.size()), alokatorius(alloc)  {
        elem = alokatorius.allocate(init.size());
        std::move(init.begin(),init.end(),elem);
    }
    ~Vector(){
        for (auto i = begin(); i != end(); i++){
            std::allocator_traits<Allocator>::destroy(alokatorius, i);
        }
        alokatorius.deallocate(elem, sz);
    }

    // Funkcijos
    Vector& operator=(const Vector& other){
        if (&other == this)      return *this;
        if (std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value){
            alokatorius = other.alokatorius;
        }
        if (alokatorius != other.alokatorius){
            alokatorius.deallocate(elem, sz);
            alokatorius = other.alokatorius;
            elem = alokatorius.allocate(other.sz);
        }
        sz = other.sz;
        cap = other.cap;
        std::copy(other.elem,other.elem + sz,elem);
        return *this;
    }
    Vector& operator=(Vector&& other) noexcept {
        if (&other == this)      return *this;
        alokatorius.deallocate(elem, sz);
        alokatorius = other.alokatorius;
        elem = other.elem;
        sz = other.sz;
        cap = other.cap;
        other.elem = nullptr;
        other.sz = 0;
        return *this;
    }
    Vector &operator=(std::initializer_list<T> ilist){
        alokatorius.deallocate(elem, sz);
        elem = alokatorius.allocate(ilist.size());
        sz = ilist.size();
        cap = ilist.size();
        std::copy(ilist.begin(),ilist.end(),elem);
        return *this;
    }
    void assign(size_type count, const T& value){
        alokatorius.deallocate(elem, sz);
        elem = alokatorius.allocate(count);
        std::fill(elem, elem + count, value);
        sz = count;
        cap = count;
    }
    void assign(iterator first, iterator last){
        auto count = std::distance(first, last);
        alokatorius.deallocate(elem, sz);
        elem = alokatorius.allocate(count);
        std::copy(first,last,elem);
        sz = count;
        cap = count;
    }
    void assign(std::initializer_list<T> ilist){
        alokatorius.deallocate(elem, sz);
        elem = alokatorius.allocate(ilist.size());
        std::copy(ilist.begin(),ilist.end(),elem);
        sz = ilist.size();
        cap = ilist.size();
    }
    allocator_type get_allocator() const{
        return alokatorius;
    }

    // Duomenu pasiekimas
    reference at(size_type pos){
        if (pos >= sz) throw std::out_of_range{"Vector::at"};
        return elem[pos];
    }
    const_reference at(size_type pos) const{
        if (pos >= sz) throw std::out_of_range{"Vector::at"};
        return elem[pos];
    }
    reference operator[](size_type pos){ return elem[pos]; }
    const_reference operator[](size_type pos) const{ return elem[pos]; }
    reference front() { return *elem; }
    const_reference front() const { return *elem; }
    reference back() { return *(elem + sz - 1); }
    const_reference back() const { return *(elem + sz - 1); }
    T* data() noexcept { return elem; }
    const T* data() const noexcept { return elem; }

    //Iteratoriai
    iterator begin() noexcept{ return elem; }
    const_iterator cbegin() const noexcept{ return elem; }
    iterator end() noexcept{ return elem + sz; }
    const_iterator cend() const noexcept{ return elem + sz; }
    reverse_iterator rbegin() noexcept{ return reverse_iterator(elem + sz - 1); }
    const_reverse_iterator crbegin() const noexcept{ return reverse_iterator(elem + sz - 1); }
    reverse_iterator rend() noexcept{ return reverse_iterator(elem - 1); }
    const_reverse_iterator crend() const noexcept{ return reverse_iterator(elem - 1); }

    //Capacity
    bool empty() const noexcept{ return sz == 0; }
    size_type size() const noexcept{ return sz; }
    size_type max_size() const noexcept{ return alokatorius.max_size(); }
    size_type capacity() const noexcept{ return cap; }
    void reserve(size_type new_cap){
        if (new_cap > max_size()) throw std::length_error("Vector::reserve(size_type new_cap)");
        if (new_cap > cap) {
            value_type* p = alokatorius.allocate(new_cap);
            std::move(begin(),end(),p);
            alokatorius.deallocate(elem, cap);
            elem = p;
            cap = new_cap;
        }
    }
    void shrink_to_fit(){
        if (sz != cap){
            value_type* p = alokatorius.allocate(sz);
            std::move(begin(),end(),p);
            alokatorius.deallocate(elem, cap);
            elem = p;
            cap = sz;
        }
    }


    // Modifiers
    void clear() noexcept{
        for (auto i = begin(); i != end(); i++) std::allocator_traits<Allocator>::destroy(alokatorius, i);
        sz = 0;
    }
    iterator insert( const_iterator pos, const T& value ){
        auto posID = std::distance(cbegin(),pos);
        if (sz == cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            *(new_elem + posID) = value;
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + 1);
            *(elem + posID) = value;
        }
        sz++;
        return &elem[posID];
    }
    iterator insert( const_iterator pos, T&& value ){
        auto posID = std::distance(cbegin(),pos);
        if (sz == cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            *(new_elem + posID) = std::move(value);
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + 1);
            *(elem + posID) = std::move(value);
        }
        sz++;
        return &elem[posID];
    }
    iterator insert( const_iterator pos, size_type count, const T& value ){
        auto posID = std::distance(cbegin(),pos);
        if (sz + count > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            std::fill(new_elem + posID, new_elem + posID + count, value);
            std::move(elem + posID, elem + sz, new_elem + posID + count);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem+sz, elem + posID + count);
            std::fill(elem + posID, elem + posID + count, value);
        }
        sz += count;
        return &elem[posID];
    }
    iterator insert( const_iterator pos, const_iterator first, const_iterator last ){
        auto posID = std::distance(cbegin(),pos);
        auto count = std::distance(first,last);
        if (sz + count > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            std::move(first,last,new_elem+posID);
            std::move(elem + posID, elem + sz, new_elem + posID + count);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + count);
            std::move(first,last, elem+posID);
        }
        sz += count;
        return &elem[posID];
    }
    iterator insert( const_iterator pos, std::initializer_list<T> ilist ){
        auto posID = std::distance(cbegin(),pos);
        if (sz + ilist.size() > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem, elem+posID, new_elem);
            std::move(ilist.begin(), ilist.end(), new_elem+posID);
            std::move(elem + posID, elem + sz, new_elem + posID + ilist.size());
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + ilist.size());
            std::move(ilist.begin(), ilist.end(), elem+posID);
        }
        sz += ilist.size();
        return &elem[posID];
    }
    template< class... Args > iterator emplace( const_iterator pos, Args&&... args ) {
        auto posID = std::distance(cbegin(),pos);
        if (sz == cap) {
            if (cap == 0) cap = 1;
            else cap *= 2;
            T *new_elem = alokatorius.allocate(cap);
            std::move(cbegin(), pos, new_elem);
            alokatorius.construct(new_elem + posID, std::forward<Args>(args)...);
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else {
            std::move(elem + posID, elem + sz, elem + posID + 1);
            alokatorius.construct(elem + posID, std::forward<Args>(args)...);
        }
        sz++;
        return &elem[posID];
    }

    iterator insert( size_type posID, const T& value ){
        if (sz == cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            *(new_elem + posID) = value;
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + 1);
            *(elem + posID) = value;
        }
        sz++;
        return &elem[posID];
    }
    iterator insert( size_type posID, T&& value ){
        if (sz == cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            *(new_elem + posID) = std::move(value);
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + 1);
            *(elem + posID) = std::move(value);
        }
        sz++;
        return &elem[posID];
    }
    iterator insert( size_type posID, size_type count, const T& value ){
        if (sz + count > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            std::fill(new_elem + posID, new_elem + posID + count, value);
            std::move(elem + posID, elem + sz, new_elem + posID + count);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem+sz, elem + posID + count);
            std::fill(elem + posID, elem + posID + count, value);
        }
        sz += count;
        return &elem[posID];
    }
    iterator insert( size_type posID, const_iterator first, const_iterator last ){
        auto count = std::distance(first,last);
        if (sz + count > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem,elem+posID,new_elem);
            std::move(first,last,new_elem+posID);
            std::move(elem + posID, elem + sz, new_elem + posID + count);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + count);
            std::move(first,last, elem+posID);
        }
        sz += count;
        return &elem[posID];
    }
    iterator insert( size_type posID, std::initializer_list<T> ilist ){
        if (sz + ilist.size() > cap){
            if (cap == 0) cap = 1;
            else cap *= 2;
            T* new_elem = alokatorius.allocate(cap);
            std::move(elem, elem+posID, new_elem);
            std::move(ilist.begin(), ilist.end(), new_elem+posID);
            std::move(elem + posID, elem + sz, new_elem + posID + ilist.size());
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else{
            std::move(elem + posID, elem + sz, elem + posID + ilist.size());
            std::move(ilist.begin(), ilist.end(), elem+posID);
        }
        sz += ilist.size();
        return &elem[posID];
    }
    template< class... Args > iterator emplace( size_type posID, Args&&... args ) {
        if (sz == cap) {
            if (cap == 0) cap = 1;
            else cap *= 2;
            T *new_elem = alokatorius.allocate(cap);
            std::move(elem, elem + posID, new_elem);
            alokatorius.construct(new_elem + posID, std::forward<Args>(args)...);
            std::move(elem + posID, elem + sz, new_elem + posID + 1);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else {
            std::move(elem + posID, elem + sz, elem + posID + 1);
            alokatorius.construct(elem + posID, std::forward<Args>(args)...);
        }
        sz++;
        return &elem[posID];
    }

    iterator erase( const_iterator pos ){
        auto posID = std::distance(cbegin(),pos);
        alokatorius.destroy(pos);
        std::move(elem + posID + 1, elem + sz, elem + posID);
        sz--;
        return &elem[posID];
    }
    iterator erase( size_type posID ){
        alokatorius.destroy(elem+posID);
        std::move(elem + posID + 1, elem + sz, elem + posID);
        sz--;
        return &elem[posID];
    }
    iterator erase( const_iterator first, const_iterator last ){
        auto count = std::distance(first,last);
        auto posID = std::distance(cbegin(),first);
        for (auto i = first; i != last; i++) alokatorius.destroy(i);
        std::move(elem + posID + count, elem + sz, elem + posID);
        sz -= count;
    }

    void push_back( T&& value ){ emplace_back(std::move(value)); }
    void push_back( const T& value ){ emplace_back(value); }
    
    template< class... Args > reference emplace_back( Args&&... args ){
        if (sz == cap) {
            if (cap == 0) cap = 1;
            else cap *= 2;
            T *new_elem = alokatorius.allocate(cap);
            std::move(elem, elem + sz, new_elem);
            alokatorius.construct(new_elem + sz, std::forward<Args>(args)...);
            alokatorius.deallocate(elem, sz);
            elem = new_elem;
        } else {
            alokatorius.construct(elem + sz, std::forward<Args>(args)...);
        }
        sz++;
        return elem[sz-1];
    }
    void pop_back(){
        sz--;
        alokatorius.destroy(elem + sz);
    }
    void resize( size_type count ){
        if (sz > count){
            sz = count;
        }
        else if (sz < count){
            if (count > max_size()) throw std::length_error("Vector::resize(size_type count)");
            else if (cap < count){
                while (cap < count) cap *= 2;
                T* new_elem = alokatorius.allocate(cap);
                std::move(elem,elem + sz,new_elem);
                std::fill(new_elem + sz,new_elem + cap, T{});
                alokatorius.deallocate(elem, sz);
                elem = new_elem;
                sz = count;
            }
        }
    }
    void resize( size_type count, const value_type& value ){
        if (sz > count){
            sz = count;
        }
        else if (sz < count){
            if (count > max_size()) throw std::length_error("Vector::resize(size_type count)");
            else if (cap < count){
                while (cap < count) cap *= 2;
                T* new_elem = alokatorius.allocate(cap);
                std::move(elem,elem + sz,new_elem);
                std::fill(new_elem + sz,new_elem + cap, value);
                alokatorius.deallocate(elem, sz);
                elem = new_elem;
                sz = count;
            }
        }
    }

    void print(std::ostream &out){
        for (int i = 0; i < sz; ++i) {
            out << elem[i] << std::endl;
        }
        //out << std::endl;
    }
};

template< class T, class Alloc > bool operator==( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    if (lhs.sz == rhs.sz) {
        for (int i = 0; i < lhs.sz; ++i) {
            if (lhs[i] != rhs[i]) return false;
        }
        return true;
    } else return false;
}
template< class T, class Alloc > bool operator!=( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    return !operator==(lhs,rhs);
}
template< class T, class Alloc > bool operator<( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    return std::lexicographical_compare(lhs.cbegin(), lhs.cend(), rhs.cbegin(), rhs.cend());
}
template< class T, class Alloc > bool operator>( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    return !(operator<(lhs,rhs) || operator==(lhs,rhs));
}
template< class T, class Alloc > bool operator<=( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    return (operator<(lhs,rhs) || operator==(lhs,rhs));
}
template< class T, class Alloc > bool operator>=( const Vector<T,Alloc>& lhs, const Vector<T,Alloc>& rhs ){
    return !operator<(lhs,rhs);
}
template< class T, class Alloc > void swap( Vector<T,Alloc>& lhs, Vector<T,Alloc>& rhs ){
    Vector<T,Alloc> temp{std::move(lhs)};
    lhs = std::move(rhs);
    rhs = std::move(temp);
}



#endif //MYVECTOR_VECTOR_H
